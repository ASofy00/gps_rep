package org.example;



public class Calculator {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    public static double add ( double num , double num2){
        return num + num2;
    }

    public static double subtract ( double num , double num2){
        return num - num2;
    }

    public static double multiply ( double num , double num2){
        return num * num2;
    }

    public static double divide ( double num , double num2){
        return num / num2;
    }
}