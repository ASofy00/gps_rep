import org.example.Calculator;

import static org.junit.jupiter.api.Assertions.*;
public class CalculatorTest {

    @org.junit.jupiter.api.Test
    void add() {
        assertEquals(7.0, Calculator.add(6,1));
    }

    @org.junit.jupiter.api.Test
    void subtract() {
        assertEquals(5.0,Calculator.subtract(6,1));
    }

    @org.junit.jupiter.api.Test
    void multiply() {
        assertEquals(10.0, Calculator.multiply(5,2));
    }

    @org.junit.jupiter.api.Test
    void divide() {
        assertEquals(3.0,Calculator.divide(9,3));
    }

}
